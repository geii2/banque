using System;

namespace Banque
{
  class Compte
  {
    public static double Maximum;
    private static double RetraitMaximum = 40;

    private static double _change = 1.12;
    public static double change
    {
      get { return _change; }
      set
      {
        if (value <= 0)
        {
          throw new ArgumentOutOfRangeException("La valeur du taux de change est invalide");
        }
        else if (Math.Abs(1 - value / _change) < 0.1)
        {
          _change = value;
        }
        else
        {
          throw new Exception("Le taux de change ne peut varier de plus de 10%");
        }
      }
    }


    public enum Resultat
    {
      Reussi = 1,
      SoldeInsuffisant = 2,
      RetraitTropImportant,
      SoldeMaximum,
      MontantInvalide
    }

    private double _solde; /* variable masquée c'est elle qui contient la valeur */
    public double solde
    { /* variable de lecture et d'ecriture */
      get
      {
        return _solde * change;
      }
      private set
      {
        _solde = value;
      }
    }
    /* getter setter

      solde = 100
      ->   set(value = 100)
      ->   _solde = value

      Console.WriteLine(solde)
      ->   get
      ->   _solde * change

    */

    public long numero { get; private set; }
    private Client client;

    /*
    private void EcrireSolde(nouveausolde) // Setter
    {
      solde = nouveausolde;
    }

    public double LireSolde() // Getter
    {
      return solde;
    }
    */

    public Compte(long numero, double solde, Client client)
    {
      this.numero = numero;
      this.solde = solde;
      this.client = client;
    }

    public Compte(Client client)
    {
      this.numero = new Random().Next(999999);
      this.solde = 10;
      this.client = client;
    }

    public Compte(long numero, Client client) : this(numero, 0.0, client)
    {
      /*
      this.numero = numero;
      this.solde = 0.0;
      this.client = client;
      */
    }

    public string Afficher()
    {
      return client + " vous avez " + solde + " euros sur votre compte " + numero;
    }

    public override string ToString() /* Méthode Afficher en mode texte */
    {
      return client + " vous avez " + solde + " euros sur votre compte " + numero;
    }

    public Resultat Deposer(double montant)
    {
      if (solde + montant <= Compte.Maximum)
      {
        solde = solde + montant;
        return Resultat.Reussi;
      }
      else
      {
        return Resultat.SoldeMaximum;
      }
    }

    public Resultat Retirer(double montant)
    {
      if (montant > RetraitMaximum)
      {
        return Resultat.RetraitTropImportant;
      }
      else if (montant < solde)
      {
        if (montant < 0)
        {
          return Resultat.MontantInvalide;
        }

        solde = solde - montant;
        return Resultat.Reussi;
      }
      else
      {
        return Resultat.SoldeInsuffisant;
      }
    }
  }
}
