using System;
using System.Collections.Generic;

namespace Banque
{
  class CompteController
  {
    static List<Compte> liste = new List<Compte>();
    static public bool Choix(ConsoleKey touche)
    {
      switch (touche)
      {
        case ConsoleKey.D5:
          return false;

        default:
          break;
      }

      Menu();
      return true;
    }

    static public void Menu()
    {
      Console.Clear();
      Console.WriteLine("┌────────────────────────────────────────┐");
      Console.WriteLine("│ Compte                                 │");
      Console.WriteLine("│ 1 Créer                                │");
      Console.WriteLine("│ 2 Lister                               │");
      Console.WriteLine("│ 3 Exporter                             │");
      Console.WriteLine("│ 4 Importer                             │");
      Console.WriteLine("│ 5 Retour au menu principal             │");
      Console.WriteLine("└────────────────────────────────────────┘");
    }
  }
}
