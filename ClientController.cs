using System;
using System.Collections.Generic;

namespace Banque
{
  class ClientController
  {

    static List<Client> liste = new List<Client>();
    static public bool Choix(ConsoleKey touche)
    {
      switch (touche)
      {
        case ConsoleKey.D1:
          Creer();
          break;

        case ConsoleKey.D2:
          Lister();
          break;

        case ConsoleKey.D5:
          return false;

        default:
          break;
      }

      Menu();
      return true;
    }

    static public void Menu()
    {
      Console.Clear();
      Console.WriteLine("┌────────────────────────────────────────┐");
      Console.WriteLine("│ Client                                 │");
      Console.WriteLine("│ 1 Créer                                │");
      Console.WriteLine("│ 2 Lister                               │");
      Console.WriteLine("│ 3 Exporter                             │");
      Console.WriteLine("│ 4 Importer                             │");
      Console.WriteLine("│ 5 Retour au menu principal             │");
      Console.WriteLine("└────────────────────────────────────────┘");
    }

    private static void Creer()
    {
      Console.Clear();

      Console.Write("Le prénom : ");
      string Prénom = Console.ReadLine();

      Console.Write("Le nom : ");
      string Nom = Console.ReadLine();

      Client client = new Client(Prénom, Nom);
      liste.Add(client);

      Console.ReadKey(true);
    }

    private static void Lister()
    {
      Console.Clear();
      foreach (Client client in liste)
      {
        Console.WriteLine(client);
      }
      Console.ReadKey(true);
    }
  }
}
