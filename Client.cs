using System;

namespace Banque
{
  class Client
  {
    public string Nom { get; private set; }

    public string Prénom { get; private set; }


    public DateTime Naissance { get; private set; }

    public Client(string Prenom, string Nom)
    {
      this.Prénom = Prénom;
      this.Nom = Nom;
    }

    public override string ToString()
    {
      return Prénom + " " + Nom;
    }
  }
}
