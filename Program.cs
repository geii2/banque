﻿using System;

namespace Banque
{
  class Program
  {
    static string controller = "";

    static bool MenuGerer()
    {
      ConsoleKeyInfo c = Console.ReadKey(true);

      switch (controller)
      {
        case "client":
          if (!ClientController.Choix(c.Key))
          {
            MenuPrincipal();
          }
          break;

        case "compte":
          if (!ClientController.Choix(c.Key))
          {
            MenuPrincipal();
          }
          break;

        default:
          return Choix(c.Key);
      }

      return true;
    }

    static bool Choix(ConsoleKey touche)
    {
      switch (touche)
      {
        case ConsoleKey.D1:
          controller = "client";
          ClientController.Menu();
          break;

        case ConsoleKey.D2:
          controller = "compte";
          CompteController.Menu();
          break;

        case ConsoleKey.D3:
          return false;

        default:
          break;
      }

      return true;
    }

    static void MenuPrincipal()
    {
      controller = "";
      Console.Clear();
      Console.WriteLine("┌────────────────────────────────────────┐");
      Console.WriteLine("│ Automate Bancaire                      │");
      Console.WriteLine("│ 1 Client                               │");
      Console.WriteLine("│ 2 Compte                               │");
      Console.WriteLine("│ 3 Quitter                              │");
      Console.WriteLine("│                                        │");
      Console.WriteLine("│                                        │");
      Console.WriteLine("└────────────────────────────────────────┘");
    }

    static void Main(string[] args)
    {
      MenuPrincipal();
      do
      {
      } while (MenuGerer());
    }

  }

}
